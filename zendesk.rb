require 'faraday'
require 'faraday/retry'
require 'json'

class Zendesk
  def initialize
    init_sand
    init_prod
  end

  def get_sand(endpoint, params = nil)
    get @conn_sand, endpoint, params
  end

  def get_prod(endpoint, params = nil)
    get @conn_prod, endpoint, params
  end

  private

  def init_sand
    @conn_sand = Faraday.new(ENV.fetch('ZD_SB_URL')) do |config|
      config.request :retry, retry_options
      config.response :json
      config.request :authorization, :basic, "#{ENV.fetch('ZD_SB_USERNAME')}/token", ENV.fetch('ZD_SB_TOKEN')
    end
  end

  def init_prod
    @conn_prod = Faraday.new(ENV.fetch('ZD_URL')) do |config|
      config.request :retry, retry_options
      config.response :json
      config.request :authorization, :basic, "#{ENV.fetch('ZD_USERNAME')}/token", ENV.fetch('ZD_TOKEN')
    end
  end

  def retry_options
    { retry_statuses: [429] } # Retry when hitting rate limit
  end

  def handle_body(resp)
    resp.body
  end

  def handle_status(resp)
    if resp.status != 200
      puts "FAIL: Status #{resp.status}"
      exit 2
    end
  end

  def get(connection, endpoint, params = nil)
    resp = connection.get endpoint, params
    handle_status resp
    handle_body resp
  end
end
