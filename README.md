# ZTheme

POC\
WIP\

Process templated Zendesk theme

Markers:

- `[[ form: abc ]]`
- `[[ field: def ]]`

Spaces in markers, after `[[`, after `:`, and before`]]`, are allowed.\

Form and field values can have any character, including spaces.

Some valid markers:

- `[[form:abc]]`
- `[[ form: abc ]]`
- `[[ form: a b c! ]]`

Test marker detection with:

```sh
rspec --format doc
```

## TODO

- field translation
- ZD API paging
- currently translates a fix set of files, add commandline translation?
- logging?
- do not process comment lines (1st char is `#`) ?
- change matching strategy: match `[[*]]` and then validate & split in code
  - much simpler regex
  - solid error catching

No AI/LLMs were involved, all mistakes are my own
