
# frozen_string_literal: true

class Marker
  def initialize(forms)
    @forms = forms
  end

  def replace(match)
    # [0] = original marker in text
    # [1] = 'form' or 'field'
    # [2] = value
    @text = match[0]
    @type = match[1]
    @value = match[2]
    case @type
    when 'form'
      replace_form
    when 'field'
      replace_field
    else
      abort "ERROR: Unknown marker field type #{@type}"
    end
  end

  def replace_form
    sand_text = @forms.lookup_sand(@value).to_s
    prod_text = @forms.lookup_prod(@value).to_s
    return sand_text, prod_text
  end

  def replace_field
    return 'sand777', 'prod777'
  end
end