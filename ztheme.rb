#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'line'
require_relative 'form'

FILES = [
  { input: 'play.in', output_sand: 'play.out.sand', output_prod: 'play.out.prod' }
].freeze

class Ztheme
  def initialize(files)
    @files = files
    initialize_lookup
  end

  def initialize_lookup
    @forms = Form.new
  end

  def process
    @files.each do |file|
      process_file(file[:input], file[:output_sand], file[:output_prod])
    end
  end

  def process_file(file_in, file_out_sand, file_out_prod)
    output_sand = File.open(file_out_sand, 'w')
    output_prod = File.open(file_out_prod, 'w')
    File.foreach(file_in) do |text|
      transform(text, output_sand, output_prod)
    end
    output_sand.close
    output_prod.close
  end

  def transform(text, output_sand, output_prod)
    line = Line.new(text, @forms)
    new_sand_text, new_prod_text = line.process
    save(new_sand_text, output_sand)
    save(new_prod_text, output_prod)
  end

  def save(text, output)
    output.write text
  end
end

ztheme = Ztheme.new(FILES)
ztheme.process
