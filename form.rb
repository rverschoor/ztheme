# frozen_string_literal: true

require_relative 'zendesk'

class Form
  def initialize
    @zd = Zendesk.new
    get_forms_sand
    get_forms_prod
  end

  def get_forms_sand
    puts '--- Sandbox ---'
    @forms_sand = {}
    json = @zd.get_sand 'ticket_forms'
    json['ticket_forms'].each do |form|
      @forms_sand[form['name'].downcase] = form['id']
    end
    pp @forms_sand
    puts "next_page: #{json['next_page']}" # TODO: paging
  end

  def get_forms_prod
    puts '--- Production ---'
    @forms_prod = {}
    json = @zd.get_prod 'ticket_forms'
    json['ticket_forms'].each do |form|
      @forms_prod[form['name'].downcase] = form['id']
    end
    pp @forms_prod
    puts "next_page: #{json['next_page']}" # TODO: paging
  end

  def lookup_sand(value)
    result = @forms_sand[value]
    puts "sand '#{value}' => '#{result}'"
    result
  end

  def lookup_prod(value)
    result = @forms_prod[value]
    puts "prod '#{value}' => '#{result}'"
    result
  end
end