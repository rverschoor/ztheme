# frozen_string_literal: true

require_relative '../line'

shared_examples 'matching' do |text, result|
  it 'match line with marker' do
    puts "marker '#{text}' should be #{result}"
    @line.instance_variable_set(:@text, text)
    expect(@line.has_marker?).to be result
  end
end

shared_examples 'extracting' do |text, result|
  it 'extract marker' do
    puts "marker '#{text}' should extract to #{result}"
    @line.instance_variable_set(:@text, text)
    markers = @line.extract_markers
    markers.each_with_index do |marker, idx|
      expect(marker[0]).to eq result[idx][0]
      expect(marker[1]).to eq result[idx][1]
      expect(marker[2]).to eq result[idx][2]
    end
  end
end

describe Line do
  before(:all) do
    @line = Line.new('bogusText', 'bogusForms')
  end
  
  context 'marker matching' do
    include_examples 'matching', '', false
    include_examples 'matching', '[[ ]]', false
    include_examples 'matching', '[[ abc ]]', false
    include_examples 'matching', '[[foo: abc]]', false
    include_examples 'matching', '[[form:]]', false
    include_examples 'matching', '[[form: ]]', false
    include_examples 'matching', '[form: abc]', false
    include_examples 'matching', 'form: abc', false
    include_examples 'matching', '[[form: abc', false
    include_examples 'matching', 'form: abc]]', false
    include_examples 'matching', '[[.form: abc.]]', false
    include_examples 'matching', '[[form: abc]]', true
    include_examples 'matching', '[[field: abc]]', true
    include_examples 'matching', '[[ form: abc]]', true
    include_examples 'matching', '[[form:    abc]]', true
    include_examples 'matching', '[[form: abc ]]', true
    include_examples 'matching', '[[ form: abc ]]', true
    include_examples 'matching', '.[[form:abc]].', true
    include_examples 'matching', '[[ form:abc ]]', true
    include_examples 'matching', '[[ form:abc ]][[ form:def ]]', true
    include_examples 'matching', '[[ form:abc ]] [[ form:def ]]', true
    include_examples 'matching', '[[ form:abc ]].[[ form:def ]]', true
    include_examples 'matching', '[[ form:abc def]]', true
    include_examples 'matching', '[[ form: abc def]]', true
    include_examples 'matching', '[[ form: abc def ]]', true
  end

  context 'marker extracting' do
    include_examples 'extracting', '[[form: abc]]', [['[[form: abc]]', 'form', 'abc']]
    include_examples 'extracting', '[[form: abc def]]', [['[[form: abc def]]', 'form', 'abc def']]
    include_examples 'extracting', '[[form: abc def ]]', [['[[form: abc def ]]', 'form', 'abc def']]
    include_examples 'extracting', '[[form: abc "def"]]', [['[[form: abc "def"]]', 'form', 'abc "def"']]
    include_examples 'extracting', '[[form: abc]] [[form: def]]', [['[[form: abc]]', 'form', 'abc'], ['[[form: def]]', 'form', 'def']]
    include_examples 'extracting', '[[form: abc]]+[[form: def]]', [['[[form: abc]]', 'form', 'abc'], ['[[form: def]]', 'form', 'def']]
    include_examples 'extracting', '[[form: abc]] [[field: def]]', [['[[form: abc]]', 'form', 'abc'], ['[[field: def]]', 'field', 'def']]
  end
end
