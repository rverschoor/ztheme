# frozen_string_literal: true

require_relative 'marker'

class Line
  # MARKER = /(\[\[ *(form|field): *(.+?)\]\])/
  MARKER = /(\[\[ *(form|field):\s*(\S.+?)\]\])/

  def initialize(text, forms)
    @text = text
    @forms = forms
    @marker = Marker.new(@forms)
  end

  def process
    if has_marker?
      process_markers
    else
      return @text, @text
    end
  end

  def has_marker?
    !!(@text =~ MARKER)
  end

  def process_markers
    matches = extract_markers
    replace_markers matches
  end

  def extract_markers
    markers = @text.scan(MARKER)
    markers.each do |marker|
      marker[2] = marker[2].strip.downcase  # [2] = value
    end
    markers
  end

  def replace_markers(matches)
    new_text_sand = @text
    new_text_prod = @text
    matches.each do |match|
      new_value_sand, new_value_prod = @marker.replace(match)
      # [0] = original marker in text
      new_text_sand = new_text_sand.gsub(match[0], new_value_sand)
      new_text_prod = new_text_prod.gsub(match[0], new_value_prod)
    end
    return new_text_sand, new_text_prod
  end
end
